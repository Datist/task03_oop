package com.Shyra;

public class Gpu extends PcHardware {
    private int frequency;
    private int memory;

    public Gpu(String name, String function, String manufacturer, int tdp, int frequency, int memory) {
        super(name, function, manufacturer, tdp);
        this.frequency = frequency;
        this.memory = memory;
    }


    @Override
    public String toString() {
        return "Gpu{" + super.toString() +
                "frequency=" + frequency +
                ", memory=" + memory +
                '}';
    }
}
