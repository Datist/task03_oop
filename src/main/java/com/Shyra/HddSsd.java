package com.Shyra;

public class HddSsd extends PcHardware {
    private int memory;
    private int readSpeed;
    private int writeSpeed;

    public HddSsd(String name, String function, String manufacturer, int tdp, int memory, int readSpeed, int writeSpeed) {
        super(name, function, manufacturer, tdp);
        this.memory = memory;
        this.readSpeed = readSpeed;
        this.writeSpeed = writeSpeed;
    }

    @Override
    public String toString() {
        return "HddSsd{" + super.toString() +
                "memory=" + memory +
                ", readSpeed=" + readSpeed +
                ", writeSpeed=" + writeSpeed +
                '}';
    }
}

