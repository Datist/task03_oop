package com.Shyra;


import java.util.*;
import java.lang.*;
import java.io.*;

public class PcHardware {
    private String name;
    private String function;
    private String manufacturer;
    private int tdp;

    public PcHardware(String name, String function, String manufacturer, int tdp) {
        this.name = name;
        this.function = function;
        this.manufacturer = manufacturer;
        this.tdp = tdp;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFunction() {
        return function;
    }

    public void setFunction(String function) {
        this.function = function;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public int getTdp(){
        return tdp;
    }

    public void setTdp(int tdp) {
        this.tdp = tdp;
    }



    @Override
    public String toString() {
        return "PcHardware " +
                "name=" + name + '\'' +
                ", function=" + function + '\'' +
                ", manufacturer=" + manufacturer + '\'' +
                ", tdp=" + tdp + "W" + '\'';
    }

    static class SortByKeyword implements Comparator<PcHardware>
    {
        // Used for sorting in ascending order of
        // roll number
        public int compare(PcHardware o1, PcHardware o2) {
            return o1.tdp - o2.tdp;
        }
    }
}
