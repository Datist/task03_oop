package com.Shyra;

public class Cpu extends PcHardware {
    private int frequency;
    private int cores;
    private int threds;

    public Cpu(String name, String function, String manufacturer, int tdp, int frequency, int cores, int threds) {
        super(name, function, manufacturer, tdp);
        this.frequency = frequency;
        this.cores = cores;
        this.threds = threds;
    }

    @Override
    public String toString() {
        return "Cpu{" + super.toString() +
                "frequency=" + frequency +
                ", cores=" + cores +
                ", threds=" + threds +
                '}';
    }
}
