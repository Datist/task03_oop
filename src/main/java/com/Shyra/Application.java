package com.Shyra;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Application {

    public static void main(String[] args) {
        Gpu myGpu = new Gpu("GTX 1060 6g", "Graphics Proccesing", "Asus", 120,
                1506, 6144);
        Cpu myCpu = new Cpu("AMD FX 6300", "Command interpretation", "AMD", 95,
                4100, 6, 6);
        Ram myRam1 = new Ram("Kingston", "High speed memory", "Kingston Technology", 1, 2048, 1333);
        Ram myRam2 = new Ram("Hunix", "High speed memory", "Kingston Technology", 1, 4196, 1333);
        Ram myRam3 = new Ram("Noname", "High speed memory", "Kingston Technology", 1, 4196, 1333);
        HddSsd mySsd = new HddSsd("KINGSTON SHFS37A120G", "long term memory", "Kingston", 1, 122880, 500, 500);
        HddSsd myHdd = new HddSsd("SAMSUNG HD321KJ", "long term memory", "Samsung", 5, 327680, 120, 80);
        List<PcHardware> dataBase = new ArrayList<>();
        dataBase.add(myCpu);
        dataBase.add(myGpu);
        dataBase.add(myHdd);
        dataBase.add(mySsd);
        dataBase.add(myRam1);
        dataBase.add(myRam2);
        dataBase.add(myRam3);




        System.out.println("Unsorted");
        for (int i=0; i<dataBase.size(); i++)
            System.out.println(dataBase.get(i));

        Collections.sort(dataBase, new PcHardware.SortByKeyword());

        System.out.println("\nSorted by tdp");
        for (int i=0; i<dataBase.size(); i++)
            System.out.println(dataBase.get(i));


//        System.out.println(myGpu);
//        System.out.println(myCpu);
//        System.out.println(myRam1);
//        System.out.println(myRam2);
//        System.out.println(myRam3);
//        System.out.println(mySsd);
//        System.out.println(myHdd);

        //start of test


        }


    }