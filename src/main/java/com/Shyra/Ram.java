package com.Shyra;

public class Ram extends PcHardware {
    private int memory;
    private int fryquency;

    public Ram(String name, String function, String manufacturer, int tdp, int memory, int fryquency) {
        super(name, function, manufacturer, tdp);
        this.memory = memory;
        this.fryquency = fryquency;
    }

    @Override
    public String toString() {
        return "Ram{" + super.toString() +
                "memory=" + memory +
                ", fryquency=" + fryquency +
                '}';
    }
}
